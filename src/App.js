import React from "react";
import "./App.css";
import ConfirmTransferredPage from "./confirmTransferredPage/index";

function App() {
    return (
        <div className="App">
            <ConfirmTransferredPage/>
        </div>
    );
}

export default App;
