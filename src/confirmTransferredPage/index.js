import React, { Component } from "react";
import {
    BankContent, BankIcon, Button,
    Container,
    Content, ErrorText,
    FlexContainer,
    Image, InnerContainer,
    Input,
    InputContent,
    Label, Nav,
    Root, SelectInput,
    Title, WrapBank,
    WrapImage
} from "./styled";
import DatePicker from "react-datepicker";
import moment from "moment";

import "react-datepicker/dist/react-datepicker.css";

class ConfirmTransferredPage extends Component {
    static defaultProps = {
        bankList: [ {
            id: 1,
            value: "kbank",
            logo: "/assets/images/kbank.png",
            options: [ {
                id: 1,
                value: "โอนเข้าบัญชี 1"
            }, {
                id: 2,
                value: "โอนเข้าบัญชี 2"
            }, {
                id: 3,
                value: "โอนเข้าบัญชี 3"
            } ]
        }, {
            id: 2,
            value: "scb",
            logo: "/assets/images/scb.png",
            options: [ {
                id: 1,
                value: "โอนเข้าบัญชี 1"
            }, {
                id: 2,
                value: "โอนเข้าบัตรเดบิต"
            } ]
        }, {
            id: 3,
            value: "bangkok",
            logo: "/assets/images/bangkok.png",
            options: [ {
                id: 1,
                value: "โอนเข้าบัญชีเงินเดือน"
            }, {
                id: 2,
                value: "จ่ายหนี้บัตรเครดิต"
            } ]
        }, {
            id: 4,
            value: "gsb",
            logo: "/assets/images/gsb.png",
            options: [ {
                id: 1,
                value: "โอนเข้าสลากออมสิน"
            } ]
        }, {
            id: 5,
            value: "tmb",
            logo: "/assets/images/tmb.png",
            options: [ {
                id: 1,
                value: "โอนเข้าบัญชีพนักงาน"
            }, {
                id: 2,
                value: "ฝากป้าข้างบ้านไปส่ง"
            } ]
        }, {
            id: 6,
            value: "uob",
            logo: "/assets/images/uob.png",
            options: [ {
                id: 1,
                value: "โอนเข้าบัญชีพนักงาน"
            }, {
                id: 2,
                value: "โอนเข้าบัญชีสำรอง"
            }, {
                id: 3,
                value: "ติดไว้ก่อน"
            } ]
        } ]
    };

    constructor( props ) {
        super( props );

        this.state = {
            cardId: "",
            date: new Date(),
            bank: "",
            optionSelected: "",
            idError: false,
            btnDisabled: true
        };

        this.onChange = this.onChange.bind( this );
        this.handleChange = this.handleChange.bind( this );
        this.handleChangeId = this.handleChangeId.bind( this );
        this.selectBank = this.selectBank.bind( this );
        this.findOption = this.findOption.bind( this );
        this.handleSelectDropdown = this.handleSelectDropdown.bind( this );
        this.onkeypress = this.onkeypress.bind( this );
    }

    onChange( event ) {
        const { name, value } = event.target;
        this.setState( prevState => ( {
            ...prevState,
            [ name ]: value
        } ) );
    }

    handleChange( date ) {
        this.setState( prevState => ( {
            ...prevState,
            date: date
        } ) );
    };

    handleChangeId( event ) {
        const { value } = event.target;
        if ( value.length !== 13 ) {
            this.setState( prevState => ( {
                ...prevState,
                idError: true
            } ) );
        }
        let sum = 0;
        for ( let i = 0; i < 12; i++ ) {
            sum += parseFloat( value.charAt( i ) ) * ( 13 - i );
            console.log( sum );
        }
        console.log( sum );
        if ( ( 11 - sum % 11 ) % 10 !== parseFloat( value.charAt( 12 ) ) ) {
            this.setState( prevState => ( {
                ...prevState,
                idError: true
            } ) );
        } else {
            this.setState( prevState => ( {
                ...prevState,
                idError: false
            } ) );
        }
    }

    selectBank( bank ) {
        this.setState( prevState => ( {
            ...prevState,
            bank: this.state.bank === bank ? "" : bank
        } ) );
    }

    findOption() {
        if ( this.state.bank === "" ) {
            return null;
        } else {
            return this.props.bankList.filter( i => i.value === this.state.bank );
        }
    }

    handleSelectDropdown( event ) {
        const { value } = event.target;
        this.setState( prevState => ( {
            ...prevState,
            optionSelected: value
        } ) );
    }

    checkEnableBtn() {
        const { date, cardId, idError, bank, optionSelected } = this.state;
        if ( !idError && bank !== "" && cardId !== "" && date !== "" && optionSelected !== "" ) {
            return false;
        } else {
            return true;
        }
    }

    onkeypress( event ) {
        let regex = new RegExp( "^[0-9]*$" );
        let key = String.fromCharCode( !event.charCode ? event.which : event.charCode );

        if ( !regex.test( key ) ) {
            event.preventDefault();
            return false;
        }
    }

    submit() {
        const { date, cardId, bank, optionSelected } = this.state;
        console.log( `เลขประชาชน 13 หลัก: ${ cardId }\n​` +
            `วันที่โอน: ${ moment( date ).format( "DD/MM/YYYY" ) }\n​` +
            `ธนาคารที่โอนเงิน: ${ bank }\n` +
            `วิธีโอนเงินเข้าบัญชีพนักงาน: ${ optionSelected }` );
    }

    render() {
        const { bankList } = this.props;
        const { date, cardId, idError, bank, optionSelected } = this.state;
        return (
            <Root>
                <Nav/>
                <Container>
                    <FlexContainer>
                        <InnerContainer width="40%">
                            <WrapImage>
                                <Image style={ {
                                    backgroundImage: `url("/assets/images/logo.png")`
                                } }/>
                            </WrapImage>
                        </InnerContainer>
                        <InnerContainer width="60%">
                            <Content>
                                <Title>ข้อมูลอื่นๆ</Title>
                                <InputContent paddingBottom="0px"
                                              borderBottom="none">
                                    <Label>เลขประชาชน 13 หลัก *</Label>
                                    <Input type="text"
                                           value={ cardId }
                                           name="cardId"
                                           maxLength="13"
                                           onChange={ e => this.onChange( e ) }
                                           onKeyUp={ e => this.handleChangeId( e ) }
                                           onKeyPress={ e => this.onkeypress( e ) }/>
                                    { idError && <ErrorText>เลขประชาชนไม่ถูกต้อง</ErrorText> }
                                </InputContent>
                                <InputContent paddingBottom="20px"
                                              borderBottom="1px solid #ccc">
                                    <Label>วันที่โอน *</Label>
                                    <DatePicker
                                        selected={ date }
                                        onChange={ this.handleChange }
                                        maxDate={ new Date() }
                                        dateFormat="dd/MM/yyyy"
                                        onChangeRaw={ e => e.preventDefault() }
                                    />
                                </InputContent>
                                <BankContent>
                                    <Label>ธนาคารที่โอนเงิน:</Label>
                                    <WrapBank>
                                        {
                                            bankList.map( ( i, k ) => (
                                                <BankIcon key={ k }
                                                          onClick={ () => bank !== "" && bank !== i.value ? undefined : this.selectBank( i.value ) }>
                                                    <Image style={ {
                                                        backgroundImage: `url(${ i.logo })`
                                                    } }
                                                           filter={ bank !== "" && bank !== i.value }/>
                                                </BankIcon>
                                            ) )
                                        }
                                    </WrapBank>
                                </BankContent>
                                <InputContent paddingBottom="0px"
                                              borderBottom="none">
                                    <Label>วิธีโอนเงินเข้าบัญชีพนักงาน</Label>
                                    <SelectInput value={ optionSelected }
                                                 onChange={ e => this.handleSelectDropdown( e ) }>
                                        <option value="">โปรดระบุ</option>
                                        {
                                            this.findOption() && this.findOption()[ 0 ].options.map( ( item, k ) => (
                                                <option key={ k } value={ item.value }>{ item.value }</option>
                                            ) )
                                        }
                                    </SelectInput>
                                </InputContent>
                            </Content>
                            <Button disabled={ this.checkEnableBtn() }
                                    onClick={ () => this.checkEnableBtn() ? undefined : this.submit() }>
                                FINISH
                            </Button>
                        </InnerContainer>
                    </FlexContainer>
                </Container>
            </Root>
        );
    }
}

export default ConfirmTransferredPage;
