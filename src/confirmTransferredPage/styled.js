import styled from "styled-components";

export const Root = styled.div`
  position: relative;
  width: 100%;
  background: #f1f1f1;
  min-height: 100vh;
`;

export const Nav = styled.div`
  position: relative;
  width: 100%;
  height: 60px;
  background: #354a48;
  margin-bottom: 30px;
`;

export const Container = styled.div`
  position: relative;
  width: 100%;
  max-width: 860px;
  margin: 0 auto;
  background: #fff;
  border-radius: 4px;
  border: 1px solid #ccc;
`;

export const FlexContainer = styled.div`
  display: flex;
  align-items: stretch;
  padding: 20px;
  box-sizing: border-box;
`;

export const InnerContainer = styled.div`
  margin: 10px;
  text-align: center;
  width: ${ ( { width } ) => width };
`;

export const WrapImage = styled.div`
    position: relative;
    height: 125px;
    transform: translateY(-50%);
    top: 50%;
`;

export const Image = styled.div`
    width: 100%;
    height: 100%;
    overflow: hidden;
    position: relative;
    background: center center no-repeat;
    background-size: contain;
    filter: ${ ( { filter } ) => filter ? "grayscale(100%)" : "grayscale(0%)" };
    cursor: ${ ( { filter } ) => filter ? "not-allowed" : "pointer" };
`;

export const Content = styled.div`
    position: relative;
    width: 100%;
    text-align: left;
    margin-bottom: 40px;
    float: left;
`;

export const Title = styled.div`
    position: relative;
    width: 100%;
    float: left;
    height: 30px;
    font-size: 22px;
    font-weight: lighter;
    color: #354a48;
`;

export const InputContent = styled.div`
    position: relative;
    width: 100%;
    float: left;
    line-height: 25px;
    padding-bottom: ${ ( { paddingBottom } ) => paddingBottom };
    border-bottom: ${ ( { borderBottom } ) => borderBottom };
    
    input {
        position: relative;
        border: 1px solid #ccc;
        height: 40px;
        padding: 0 10px;
        box-sizing: border-box;
        font-size: 14px;
        border-radius: 3px;
        width: 300px;
        float: left;
    }
`;

export const Label = styled.label`
    position: relative;
    width: 100%;
    float: left;
    padding-top: 10px;
    font-weight: bold;
    font-size: 14px;
    color: #354a48;
`;

export const ErrorText = styled.label`
    position: relative;
    width: 100%;
    float: left;
    padding-top: 10px;
    font-size: 12px;
    color: red;
`;

export const Input = styled.input`
    position: relative;
    width: 100%;
    border: 1px solid #ccc;
    height: 40px;
    padding: 0 10px;
    box-sizing: border-box;
    font-size: 14px;
    border-radius: 3px;
    max-width: 300px;
    float: left;
`;

export const BankContent = styled.div`
    position: relative;
    width: 100%;
    float: left;
    line-height: 25px;
`;

export const WrapBank = styled.div`
    display: grid;
    grid-template-columns: 40px 40px 40px;
    grid-gap: 10px;
    padding: 10px;
`;

export const BankIcon = styled.div`
    width: 40px;
    height: 40px;
`;

export const Button = styled.button`
    float: left;
    position: relative;
    width: 150px;
    background: ${ ( { disabled } ) => disabled ? "gray" : "#68c371" };
    font-weight: bold;
    color: #fff;
    border: none;
    height: 40px;
    border-radius: 3px;
    cursor: ${ ( { disabled } ) => disabled ? "not-allowed" : "pointer" };
`;

export const SelectInput = styled.select`
    position: relative;
    width: 100%;
    border: 1px solid #ccc;
    height: 40px;
    padding: 0 10px;
    box-sizing: border-box;
    font-size: 14px;
    border-radius: 3px;
    max-width: 300px;
    float: left;
`;
